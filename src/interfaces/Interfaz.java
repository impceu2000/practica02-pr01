package interfaces;

import dominio.CatalogoDeRopa;
import dominio.Prenda;
import java.lang.ArrayIndexOutOfBoundsException;

public class Interfaz{
        private static CatalogoDeRopa catalogoDeRopa = new CatalogoDeRopa();

        private static void mostrarAyuda(){
                System.out.println("Las instrucciones posibles son las siguientes:");
                System.out.println("    1. Mostrar prendas: java -jar catalogoDeRopa.jar show");
                System.out.println("    2. Mostrar esta ayuda: java -jar catalogoDeRopa.jar help");
                System.out.println("    3. Añadir prenda: java -jar catalogoDeRopa.jar add <nombre> <marca> <talla> <stock>, por ejemplo, ");
                System.out.println("                      java -jar catalogoDeRopa.jar add Cortavientos ASSC XL 50");
		System.out.println("    4. Remover prenda: java -jar catalogoDeRopa.jar remove <nombre>, por ejemplo, ");
                System.out.println("                      java -jar catalogoDeRopa.jar remove Cortavientos");
		System.out.println("    5. Modificar prenda: java -jar catalogoDeRopa.jar modify <nombreAntiguo> <nombreNuevo> <marcaNueva> <tallaNueva> <stockNuevo>, por ejemplo, ");
                System.out.println("                      java -jar catalogoDeRopa.jar modify Cortavientos Chubasquero H&M M 25");
		System.out.println("    6. Generar Hoja de Calculo del Catalogo de Ropa: java -jar catalogoDeRopa.jar spreadsheet, por ejemplo, ");
                System.out.println("                      java -jar catalogoDeRopa.jar spreadsheet");
		System.out.println("    7. Mostrar Hoja de Calculo del Catalogo de Ropa: java -jar catalogoDeRopa.jar showSpreadsheet, por ejemplo, ");
                System.out.println("                      java -jar catalogoDeRopa.jar showSpreadsheet");

        }

        public static void ejecutar(String[] args){
                try
                {
                        if (args[0].equalsIgnoreCase("add"))
                                catalogoDeRopa.annadirPrenda(new Prenda(args[1], args[2], args[3], args[4]));
                        else if (args[0].equalsIgnoreCase("show"))
                                catalogoDeRopa.mostrarPrendas();
                        else if (args[0].equalsIgnoreCase("help")) mostrarAyuda();
			else if (args[0].equalsIgnoreCase("remove")) 
				catalogoDeRopa.removePrendas(args[1]);
			else if (args[0].equalsIgnoreCase("modify"))
				catalogoDeRopa.modifyPrendas(args[1], args[2], args[3], args[4], args[5]);
			else if (args[0].equalsIgnoreCase("spreadsheet"))
                                catalogoDeRopa.generarHojaDeCalculo();
			else if (args[0].equalsIgnoreCase("showSpreadsheet"))
				catalogoDeRopa.mostrarHojaDeCalculo();
                        else mostrarAyuda();
                } 
		catch(ArrayIndexOutOfBoundsException ex){
                        mostrarAyuda();
                }
        }
/**
 * Copyright [2019] [Ignacio Martín-Peña Chinchurreta]
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.

 */
}
