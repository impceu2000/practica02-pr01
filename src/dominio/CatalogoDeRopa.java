package dominio;


import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;


public class CatalogoDeRopa{
        private ArrayList<Prenda> listaPrendas = new ArrayList<>();
        private static String nombreFichero = "Prendas.txt";
	public static String nombreFicheroCSV = "Prendas.csv";

        public CatalogoDeRopa(){
                cargarDesdeFichero();
        }

        public void annadirPrenda(Prenda prenda){
                listaPrendas.add(prenda);
                volcarAFichero();
        }

        public void mostrarPrendas()
        {
                for(Prenda prenda : listaPrendas) System.out.println(prenda.generarCadenaParaMostrar());
        }

        private void volcarAFichero(){
                try{
                        FileWriter fw = new FileWriter(nombreFichero);
                        fw.write(this.toString());
                        fw.close();
                }catch(IOException ex){
                        System.err.println("Error al intentar escribir en fichero");
                }
        }

	public void generarHojaDeCalculo(){
                try{
                        FileWriter fw = new FileWriter(nombreFicheroCSV);
			fw.write("Nombre Marca Talla Stock\n");
                        fw.write(this.toString());
                        fw.close();
                }catch(IOException ex){
                        System.err.println("Error al intentar escribir en fichero");
                }
        }

	public void mostrarHojaDeCalculo(){
		try{	String cmd = "soffice Prendas.csv";
			Runtime.getRuntime().exec(cmd);

		} catch (IOException ioe) {
			System.out.println (ioe);
	}
	}
        private void cargarDesdeFichero(){
                try{
                        File fichero = new File(nombreFichero);
                        if (fichero.createNewFile()) {
                                System.out.println("Acaba de crearse un nuevo fichero");
			} else {
                                Scanner sc = new Scanner(fichero);
                                while(sc.hasNext()){
                                        listaPrendas.add(new Prenda(sc.next(), sc.next(), sc.next(), sc.next()));
                                }
                        }
                }catch(IOException ex){
                }
        }

        public String toString() {
                StringBuilder sb = new StringBuilder();
                for(Prenda prenda : listaPrendas) sb.append(prenda + "\n");
                return sb.toString();
        }

	public void removePrendas(String nombre) {
		Prenda removePrenda = new Prenda();
		removePrenda.setNombre(nombre);
		listaPrendas.remove(removePrenda);
		volcarAFichero();
	}

	public void modifyPrendas(String nombreAntiguo, String nombreNuevo, String marcaNueva, String tallaNueva, String stockNuevo) {
                removePrendas(nombreAntiguo);
                annadirPrenda(new Prenda(nombreNuevo, marcaNueva, tallaNueva, stockNuevo));
                volcarAFichero();
	}

/**
 * Copyright [2019] [Ignacio Martín-Peña Chinchurreta]
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.

 */
}
