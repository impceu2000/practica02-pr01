package dominio;

public class Prenda{
        private String nombre;
        private String marca;
        private String talla;
	private String stock;

        public void setStock(String stock) {
		this.stock = stock;
	}

	public String getStock() {
		return stock;
	}
	
	public void setNombre(String nombre) {
                this.nombre = nombre;
        }

        public String getNombre() {
                return nombre;
        }

        public String getMarca() {
                return marca;
        }

        public void setMarca(String marca) {
                this.marca = marca;
        }

        public void setTalla(String talla) {
                this.talla = talla;
        }

        public String getTalla() {
                return talla;
        }

        public Prenda(String nombre, String marca, String talla, String stock) {
                this.nombre = nombre;
                this.marca = marca;
                this.talla = talla;
		this.stock = stock;
        }

	public String generarCadenaParaMostrar() {
		return "El/La " + getNombre() + " con marca " + getMarca() + " de la talla " + getTalla() + ", le quedan " + getStock() + " unidades.";
	}

        public String toString() {
                return getNombre() + " " + getMarca() + " " + getTalla() + " " + getStock();
        }

	public Prenda() {
	}

	public boolean equals(Object objeto) {
		Prenda prenda = (Prenda) objeto;
		return nombre.equalsIgnoreCase(prenda.nombre);
	}
/**
 * Copyright [2019] [Ignacio Martín-Peña Chinchurreta]
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.

 */
}
