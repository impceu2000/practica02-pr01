# Notas para los usuarios

Estimado proveedor logistico, este programa determinara el stock de las Prendas de nuestra tienda. Por favor se determinante y cuidadoso en fichar y controlar las cantidades. Un saludo. Gracias.

## Compilación del programa

Se ejecuta la siguiente instrucción:

~~~~
make jar
~~~~

## Uso del catalogo de ropa:

Permite ejecutar las instrucciones que se muestran a continuación: 
     
1. Mostrar prendas:

~~~~
 java -jar catalogoDeRopa.jar show
~~~~

2. Mostrar ayuda: 

~~~~
java -jar catalogoDeRopa.jar help
~~~~

3. Añadir prenda:

~~~~
java -jar catalogoDeRopa.jar add <nombre> <marca> <talla> <stock>
~~~~

    por ejemplo,
~~~~
java -jar catalogoDeRopa.jar add Zapato Scalpers 39 50
~~~~

4. Eliminar prenda:

~~~~
java -jar catalogoDeRopa.jar remove <nombre> 
~~~~

    por ejemplo,
~~~~
java -jar catalogoDeRopa.jar remove Zapato 
~~~~

5. Modificar prenda:

~~~~
java -jar catalogoDeRopa.jar modify <nombreAntiguo> <nombreNuevo> <marcaNueva> <tallaNueva> <stockNuevo>
~~~~

    por ejemplo,
~~~~
java -jar catalogoDeRopa.jar add Zapato ZapatoDeAnte Zara 41 25
~~~~

6. Generar catalogo de ropa:

~~~~
java -jar catalogoDeRopa.jar spreadsheet
~~~~

    por ejemplo,
~~~~
java -jar catalogoDeRopa.jar spreadsheet
~~~~

7. Mostrar catalogo de ropa: (libreOficce)

~~~~
java -jar catalogoDeRopa.jar showSpreadsheet
~~~~

    por ejemplo,
~~~~
java -jar catalogoDeRopa.jar showSpreadsheet
~~~~


# Notas para los desarrolladores

## Generación de Javadoc

Se ejecuta la siguiete instrucción:

~~~~
make javadoc
~~~~ 

## Inspección de Javadoc

Suponiendo que tiene instalado `firefox`, se ejecuta:

~~~~
firefox htlm/index.html
~~~~

## Sobre el fichero _makefile_

Se han utilizado sentencias específicas de Linux, por tanto, sólo
ejecuta en este sistema operativo.
